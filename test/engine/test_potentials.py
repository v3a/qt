import numpy as np
import unittest
from src.engine import potentials, example


class TestPotentials(unittest.TestCase):
    def test_empty_input(self):
        a, b, c = np.zeros((0, 0)), np.zeros((0, 0)), np.zeros((0, 0))
        res = potentials.solve_open_transportation(a, b, c)
        assert len(res) == 0

    def test_transport_components(self):
        a, b, c, x0 = example.example_transportation_data()

        # first, check initial potentials calculation correctness
        u, v = potentials.calc_u_v(c, x0)
        v_expected = np.array([0, 7, 5, 1])
        u_expected = np.array([-14, -10, -20])
        assert (u_expected == u).all() and (v_expected == v).all()

        # then check potential difference correctness
        alpha = potentials.calc_alpha(x0, u, v)
        alpha_list = [(None, 21, 19, 15),
                      (None, None, None, 11),
                      (20, 27, None, None)]
        alpha_expected = np.array([np.array(row) for row in alpha_list])
        assert (alpha == alpha_expected).all()

        t = potentials.TransportTable(u=u, v=v, c=c, x0=x0, alpha=alpha)
        (i0, j0, max_diff) = potentials.find_entering_variable(t)
        assert (i0, j0) == (2, 0)

    def test_solve_transportation_potentials(self):
        _, _, c, x0 = example.example_transportation_data()
        plans = potentials.solve_transportation_potentials(c, x0)
        t_result = list(plans)[-1]
        x = t_result.x

        # `sanity check`: all values in transportation matrix non-negative?
        assert np.min([elem for elem in x.flatten() if elem is not None]) >= 0
        alpha = t_result.alpha

        # check that solution fulfills optimization criteria
        assert [c[i, j] - alpha[i, j] >= 0 for i in range(len(c)) for j in
                range(len(c[0])) if alpha[i, j] is not None]
