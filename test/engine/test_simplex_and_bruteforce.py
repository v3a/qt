import unittest
import logging
import sys
import numpy as np
from src.engine import simplex, bruteforce, canonical_utils as cu

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


class TestSimplexAndBruteForce(unittest.TestCase):
    def test_with_automatically_converted_canonical(self):
        abs_tolerance = 1e-3
        c = [8, 8, -8, -4, 4, -2, 2, 0, 0]
        A = [[1, -2, 2, 2, -2, 0, 0, 1, 0],
             [1, 2, -2, 1, -1, 1, -1, 0, 0],
             [2, 1, -1, -4, 4, 1, -1, 0, 0],
             [-1, 4, -4, 0, 0, -2, 2, 0, 1]]
        b = [6, 24, 30, -6]
        cf = cu.NpCanonicalForm(cu.CanonicalForm(A, b, c))

        logging.info("\n--- running test with pre-defined canonical form ---")
        logging.info("---- running simplex algorithm ----")
        res = simplex.simplex_method(cf, simplex.starting_vector(cf))
        assert np.max(np.abs(np.matmul(cf.A, res.x) - cf.b)) < abs_tolerance
        f = np.matmul(res.x, cf.c)
        assert abs(f + 89) < abs_tolerance

        logging.info("\n---- running brute force ----")
        x_b = bruteforce.bruteforce(cf)
        assert np.max(np.abs(np.matmul(cf.A, x_b) - cf.b)) < abs_tolerance
        f_b = np.matmul(x_b, cf.c)
        assert abs(f_b + 89) < abs_tolerance
