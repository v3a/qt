import unittest
import numpy as np
from src.engine.canonical_utils import binomial_grid, subset_by_index


class TestCanonicalUtils(unittest.TestCase):
    def test_binomial_grid(self):
        n, k = 5, 2
        bg = binomial_grid(n, k)
        assert len(bg) == n - k + 1
        assert len(bg[0]) == k + 1
        assert bg[n - k, k] == 10
        for row in range(n - k + 1):
            assert bg[row, 0] == 1
        for col in range(k + 1):
            assert bg[0, col] == 1

    def test_subset_by_index(self):
        n, k = 5, 3
        whole_set = np.array(list(range(n)))
        bg = binomial_grid(n, k)
        subsets = set()
        for idx in range(bg[-1, -1]):
            subsets.add(tuple(subset_by_index(whole_set, bg, idx)))
        expected = {(0, 1, 2), (0, 1, 3), (0, 1, 4),
                    (0, 2, 3), (0, 2, 4),
                    (0, 3, 4),
                    (1, 2, 3), (1, 2, 4),
                    (1, 3, 4),
                    (2, 3, 4)}
        assert len(expected) == len(subsets)
        for subset in expected:
            assert subset in subsets
