# qt - English reference
## Purpose
This is PyQT implementation of visualizer of *transportation problem* solution.<br>
## Prerequisites
- Python 3.6-3.8
## Build
- clone this repository: git clone https://gitlab.com/v3a/qt.git`
- From within the root folder of repository (`cd qt`) install dependencies: `pip install -r requirements.txt`
 (or similar with your system's command for Pip)
- Move to a folder `qt/src` (`cd src`) and launch the application: `python main.py`
## Usage
In the first tab, input transportation matrix, stock and need 
(you may adjust matrix size using spinboxes), then press 'next'.<br>
In the second tab you may see step-by-step solution. Navigate between step with screen buttons.
### Example
When you start the application, there is an example already in the input tab.<br>
Hit 'next' and see its three-step solution. The problem in example is of open type
with stock exceeding need, so an extra (third) column will be added to the matrix.

# Reference in Russian | Справка
Перед Вами приложение для решения открытой транспортной задачи.
## Требования
- Python 3.6-3.8
## Установка, запуск, удаление
- создайте копию репозитория: `git clone https://gitlab.com/v3a/qt.git`
- Находясь в корневой папке репозитория, установите зависимости: `pip install requirements.txt` 
- Находясь в папке `qt/src`, исполните команду: `python main.py`
Программа не оставляет следов в системе и может быть демонтирована путём удаления папки репозитория.
## Работа с приложением
- Перейдите на вкладку `ввод данных`. Настройте число строк (число строк в матрице тарифов + 1 для вектора потребностей)
 и столбцов (число столбцов в матрице тарифов + 1 для вектора запасов). Введите значения стоимостей перевозки в матрице
 тарифов (клетки на пересечении `Ai` и `Bj`), значения запасов продукта и  потребностей покупателей.
 При необходимости выставите штрафов за недопоставку или оставтье в этих клетках нули.
- нажмите экранную кнопку `обновить`. Если Вы ввели корректную задачу (для этого требуется, чтобы все запасы и потребности 
 были ненулевые), то после этого на вкладке `пошаговое решение` можно посмотреть последовательность планов перевозок от начального
 приближения до оптимального решения. Таблица при этом отображается для задачи, приведённой к закрытому виду.
- На вкладке `решение` в клетках таблицы в левом верхнем углу отображается тариф перевозки. Если клетка не 'пустая', через
вертикальную черту указано значение перевезённого груза. Последняя строка и последний столбец таблицы - значения
потенциалов в пунктах отправления и прибытия груза. В 'пустых' клетках транспортной таблицы в левом нижнем углу указана
разность потенциалов.
- кнопками `предыдущий шаг`, `следующий шаг` можно переключаться между итерациями (если они есть).
- Под таблицей показан график изменения целевой функции (приращения стоимости товара после перевозки). Точка, соответствующая
текущей итерации, выделена красным цветом.
### Пример
При старте на вкладке `ввод данных` уже есть пример транспортной задачи. <br>
Кликните `обновить` и на вкладке `пошаговое решение` Вы можете посмотреть решение
в три шага. Транспортная задача из примера относится к открытому типу с суммарными
запасами, превышающими спрос, поэтому к таблице добавляется дополнительная (третья)
колонка.

![qt](https://user-images.githubusercontent.com/23435506/77400859-e9157c00-6dbc-11ea-9876-1cd747a7ce31.png)
