import logging
import sys

from PyQt5 import QtWidgets  # type: ignore

sys.path.append('..')  # now environment can see package `src`
from src.ui.main_window import MainWindow

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


def main():
    logging.info('Starting...')

    app = QtWidgets.QApplication(sys.argv)
    win = MainWindow(flags=None)
    win.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
