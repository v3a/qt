# mypy: ignore-errors

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt


class UiMainWindow(object):
    input_table = (
        ("3.0", "5.0", "4.0", "20.0"),
        ("6.0", "3.0", "1.0", "40.0"),
        ("3.0", "2.0", "7.0", "30.0"),
        ("3.0", "2.0", "20.0", ""),
        ("0.0", "0.0", "0.0", "")
    )
    vertical_headers = ("A1", "A2", "A3", "Потребности", "Штраф")
    horizontal_headers = ("B1", "B2", "B3", "Запасы")
    vertical_output_headers = ("A1", "A2", "A3", "u_i")
    horizontal_output_headers = ("B1", "B2", "B3", "B4", "Запасы", "v_i")

    def _setup_input_table(self):
        self.inpTableWidget.setColumnCount(len(self.horizontal_headers))
        self.inpTableWidget.setRowCount(len(self.vertical_headers))

        for i_row in range(len(self.horizontal_headers)):
            item = QtWidgets.QTableWidgetItem()
            self.inpTableWidget.setHorizontalHeaderItem(i_row, item)
            item.setText(self.horizontal_headers[i_row])

        for i_col in range(len(self.vertical_headers)):
            item = QtWidgets.QTableWidgetItem()
            self.inpTableWidget.setVerticalHeaderItem(i_col, item)
            item.setText(self.vertical_headers[i_col])

        for i_row in range(len(self.input_table)):
            for i_col in range(len(self.input_table[i_row])):
                item = QtWidgets.QTableWidgetItem()
                self.inpTableWidget.setItem(i_row, i_col, item)
                item.setText(self.input_table[i_row][i_col])

    def _setup_output_table(self):
        self.outTableWidget.setMinimumSize(
            QtCore.QSize(0, 100 * len(self.vertical_headers)))
        self.outTableWidget.setColumnCount(len(self.horizontal_output_headers))
        self.outTableWidget.setRowCount(len(self.vertical_output_headers))

        for i_row in range(len(self.horizontal_output_headers)):
            item = QtWidgets.QTableWidgetItem()
            self.outTableWidget.setHorizontalHeaderItem(i_row, item)
            item.setText(self.horizontal_output_headers[i_row])

        for i_col in range(len(self.vertical_output_headers)):
            item = QtWidgets.QTableWidgetItem()
            self.outTableWidget.setVerticalHeaderItem(i_col, item)
            item.setText(self.vertical_output_headers[i_col])

        for i_row in range(len(self.horizontal_output_headers)):
            for i_col in range(len(self.vertical_output_headers)):
                item = QtWidgets.QTableWidgetItem()
                self.outTableWidget.setItem(i_row, i_col, item)

    def __init__(self, main_window):
        main_window.resize(796, 762)
        self.centralWidget = QtWidgets.QWidget(main_window)
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralWidget)
        self.tabWidget = QtWidgets.QTabWidget(self.centralWidget)
        self.inputTab = QtWidgets.QWidget()
        self.inpTabVerticalLayout = QtWidgets.QVBoxLayout(self.inputTab)
        self.inpMenuHorizLayout = QtWidgets.QHBoxLayout()
        self.inpMenuHorizLayout.setSizeConstraint(
            QtWidgets.QLayout.SetDefaultConstraint)
        self.inpNColsTextBrowser = QtWidgets.QTextBrowser(self.inputTab)
        self.inpNColsTextBrowser.setMaximumSize(QtCore.QSize(16777215, 50))
        self.inpMenuHorizLayout.addWidget(self.inpNColsTextBrowser)
        self.inpNColsSpinBox = QtWidgets.QSpinBox(self.inputTab)
        self.inpNColsSpinBox.setMinimum(1)
        self.inpNColsSpinBox.setProperty("value",
                                         len(self.horizontal_headers))
        self.inpMenuHorizLayout.addWidget(self.inpNColsSpinBox)
        self.inpNRowsTextBrowser = QtWidgets.QTextBrowser(self.inputTab)
        self.inpNRowsTextBrowser.setMaximumSize(QtCore.QSize(16777215, 44))
        self.inpMenuHorizLayout.addWidget(self.inpNRowsTextBrowser)
        self.inpNRowsSpinBox = QtWidgets.QSpinBox(self.inputTab)
        self.inpNRowsSpinBox.setMinimum(1)
        self.inpNRowsSpinBox.setProperty("value",
                                         len(self.vertical_output_headers))
        self.inpMenuHorizLayout.addWidget(self.inpNRowsSpinBox)
        self.inpRefreshBtn = QtWidgets.QPushButton(self.inputTab)
        self.inpMenuHorizLayout.addWidget(self.inpRefreshBtn)
        self.inpTabVerticalLayout.addLayout(self.inpMenuHorizLayout)
        self.tabWidget.addTab(self.inputTab, "")
        self.outputTab = QtWidgets.QWidget()
        self.outTabVerticalLayout = QtWidgets.QVBoxLayout(self.outputTab)
        self.outMenuHorisLayout = QtWidgets.QHBoxLayout()
        self.outMenuHorisLayout.setSizeConstraint(
            QtWidgets.QLayout.SetDefaultConstraint)
        self.outPrevStepBtn = QtWidgets.QPushButton(self.outputTab)
        self.outPrevStepBtn.setMaximumSize(QtCore.QSize(16777215, 50))
        self.outMenuHorisLayout.addWidget(self.outPrevStepBtn)
        self.outTextWidget = QtWidgets.QTextBrowser(self.outputTab)
        self.outTextWidget.setMaximumSize(QtCore.QSize(16777215, 50))
        self.outMenuHorisLayout.addWidget(self.outTextWidget)
        self.outNextStepBtn = QtWidgets.QPushButton(self.outputTab)
        self.outNextStepBtn.setMaximumSize(QtCore.QSize(16777215, 50))
        self.outMenuHorisLayout.addWidget(self.outNextStepBtn)
        self.outTabVerticalLayout.addLayout(self.outMenuHorisLayout)

        self.inpTableWidget = QtWidgets.QTableWidget(self.inputTab)
        self.outTableWidget = QtWidgets.QTableWidget(self.outputTab)
        self.inpTabVerticalLayout.addWidget(self.inpTableWidget)
        self.outTabVerticalLayout.addWidget(self.outTableWidget)
        self._setup_input_table()
        self._setup_output_table()

        self.outImage = QtWidgets.QLabel(self.outputTab)
        self.outImage.setAlignment(Qt.AlignCenter)
        self.outTabVerticalLayout.addWidget(self.outImage)
        self.tabWidget.addTab(self.outputTab, "")
        self.verticalLayout.addWidget(self.tabWidget)
        main_window.setCentralWidget(self.centralWidget)

        self._retranslate_ui(main_window)
        self.tabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def _retranslate_ui(self, main_window):
        main_window.setWindowTitle(
            "Транспортная задача. Решение методом потенциалов")
        self.inpNColsTextBrowser.setHtml(
            "<html><head></head><body><p align=\"center\">Число "
            "столбцов</p></body></html>")
        self.inpNRowsTextBrowser.setHtml(
            "<html><head></head><body><p align=\"center\">Число "
            "строк</p></body></html>")
        self.inpRefreshBtn.setText("Обновить")
        self.inpTableWidget.setSortingEnabled(False)
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.inputTab),
                                  "Ввод данных")
        self.outPrevStepBtn.setText("Предыдущий шаг")
        self.outTextWidget.setHtml(
            "<html><body>Решение недоступно до ввода данных<body></html>")
        self.outNextStepBtn.setText("Следующий шаг")
        self.outTableWidget.setSortingEnabled(False)
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.outputTab),
                                  "Пошаговое решение")


def launch_ui():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()
    UiMainWindow(main_window)
    main_window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    launch_ui()
