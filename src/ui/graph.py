import matplotlib.pyplot as plt  # type: ignore
import logging
import sys


def index_graph(array, index):
    plt.clf()
    for i in range(len(array)):
        if index == i:
            plt.scatter(index, array[index], c='r', s=60)
        else:
            plt.scatter(i, array[i], c='b')
    plt.ylabel("x")
    plt.xlabel("i")
    plt.grid()
    plt.savefig('graph')
    return 'graph.png'


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    file_name = index_graph([1, 2, 5, 3, -2], 1)
    logging.info(file_name)
