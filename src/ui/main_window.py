import logging
import traceback
from functools import partial, wraps
from typing import Callable, List

import numpy as np  # type: ignore
from PyQt5 import QtWidgets, QtGui, QtCore  # type: ignore
from PyQt5.QtWidgets import QTableWidgetItem, QTableWidget  # type: ignore
from PyQt5.QtWidgets import QTextBrowser, QLabel  # type: ignore

from src.engine.potentials import solve_open_transportation
from src.engine.utils import TransportTable
from src.ui import graph
from src.ui.design import UiMainWindow


def safe_call(f):
    @wraps(f)
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            logging.debug(e)
            traceback.print_exc()
            raise

    return inner


class MainWindow(QtWidgets.QMainWindow):
    # Except headers
    start_n_rows = 5
    start_n_cols = 4
    min_n_rows = 3
    min_n_cols = 2

    @safe_call
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        self._ui = UiMainWindow(self)

        self._ui.inpNRowsSpinBox.setValue(self.start_n_rows)
        self._ui.inpNColsSpinBox.setValue(self.start_n_cols)
        self._ui.inpNRowsSpinBox.setMinimum(self.min_n_rows)
        self._ui.inpNColsSpinBox.setMinimum(self.min_n_cols)

        self._in_table = self._ui.inpTableWidget
        self._out_table = self._ui.outTableWidget

        self._input = _get_input(self._in_table)

        self._i_step = 0
        self._steps = None

        self._ui.inpNRowsSpinBox.valueChanged.connect(
            partial(self._on_resize_table, method=_change_row_count))
        self._ui.inpNColsSpinBox.valueChanged.connect(
            partial(self._on_resize_table, method=_change_col_count))
        self._ui.inpRefreshBtn.clicked.connect(self._on_refresh)

        self._ui.outNextStepBtn.clicked.connect(self._on_next_step)
        self._ui.outPrevStepBtn.clicked.connect(self._on_prev_step)

    @safe_call
    def _on_resize_table(
            self, new_size: int,
            method: Callable[[np.array, int], np.array]
    ) -> None:
        self._input = _get_input(self._in_table)
        self._input = method(self._input, new_size)
        _update_input_table(self._in_table, self._input)

    @safe_call
    def _on_refresh(self, _) -> None:
        self._input = _get_input(self._in_table)
        logging.info(f'Refresh input:\n{self._input}')
        tariff = self._input[:-2, :-1]
        stock = self._input[:-2, -1].flatten()
        need = self._input[-2, :-1].flatten()
        penalty = self._input[-1, :-1].flatten()
        self._i_step = 0
        try:
            self._steps = solve_open_transportation(
                stock=stock, need=need,
                tariff=tariff, penalty=penalty
            )
            _update_out_ui(self._i_step, self._steps, self._out_table,
                           self._ui.outTextWidget, self._ui.outImage)
        except Exception:
            logging.info("unable to refresh table: "
                         "solver engine failed to process input")

    @safe_call
    def _on_next_step(self, _) -> None:
        if self._steps is None:
            logging.info('Next step is not available until refresh')
            return

        if 0 <= self._i_step + 1 < len(self._steps):
            self._i_step += 1
            _update_out_ui(self._i_step, self._steps, self._out_table,
                           self._ui.outTextWidget, self._ui.outImage)

    @safe_call
    def _on_prev_step(self, _) -> None:
        if self._steps is None:
            logging.info('Previous step is not available until refresh')
            return

        if 0 <= self._i_step - 1 < len(self._steps):
            self._i_step -= 1
            _update_out_ui(self._i_step, self._steps, self._out_table,
                           self._ui.outTextWidget, self._ui.outImage)


def _get_input(table: QTableWidget, *, default: float = 0.0) -> np.array:
    n_rows = table.rowCount()
    n_cols = table.columnCount()
    res = []
    for i_row in range(n_rows):
        row = []
        for i_col in range(n_cols):
            try:
                row.append(float(table.item(i_row, i_col).text()))
            except Exception:
                row.append(default)
        res.append(row)
    return np.array(res)


def _change_row_count(table: np.array, new_n_rows: int) -> np.array:
    logging.info(f'Changing row count to {new_n_rows}')
    if len(table) < new_n_rows:
        return _increase_rows_count(table, new_n_rows)
    else:
        return _decrease_rows_count(table, new_n_rows)


def _change_col_count(table: np.array, new_n_cols: int) -> np.array:
    logging.info(f'Changing row count to {new_n_cols}')
    if table.shape[1] < new_n_cols:
        return _increase_cols_count(table, new_n_cols)
    else:
        return _decrease_cols_count(table, new_n_cols)


def _increase_rows_count(table: np.array, new_n_rows: int) -> np.array:
    n_rows, n_cols = table.shape
    zeros = np.zeros((new_n_rows - n_rows + 1, n_cols))
    zeros[-1] = table[n_rows - 1]
    return np.concatenate((table[:n_rows - 1], zeros))


def _decrease_rows_count(table: np.array, new_n_rows: int) -> np.array:
    return np.concatenate((table[:new_n_rows - 1, :], table[-1, np.newaxis]))


def _increase_cols_count(table: np.array, new_n_cols: int) -> np.array:
    n_rows, n_cols = table.shape
    zeros = np.zeros((n_rows, new_n_cols - n_cols + 1))
    zeros[:, -1] = table[:, -1]
    return np.concatenate((table[:, :n_cols - 1], zeros), axis=1)


def _decrease_cols_count(table: np.array, new_n_cols: int) -> np.array:
    return np.concatenate((table[:, :new_n_cols - 1], table[:, -1:]), axis=1)


def _update_input_table(ui: QTableWidget, table: np.array) -> None:
    n_rows, n_cols = table.shape
    ui.setRowCount(n_rows)
    ui.setColumnCount(n_cols)
    for i_row in range(n_rows):
        for i_col in range(n_cols):
            ui.setItem(
                i_row, i_col,
                QTableWidgetItem(str(table[i_row, i_col]))
            )

    for i_row in range(n_rows):
        ui.setVerticalHeaderItem(i_row, QTableWidgetItem(f'A{i_row + 1}'))
    ui.setVerticalHeaderItem(n_rows - 2, QTableWidgetItem('Потребности'))
    ui.setVerticalHeaderItem(n_rows - 1, QTableWidgetItem('Запасы'))

    for i_col in range(n_cols):
        ui.setHorizontalHeaderItem(i_col, QTableWidgetItem(f'B{i_col + 1}'))
    ui.setHorizontalHeaderItem(n_cols - 1, QTableWidgetItem(f'Запасы'))


def _update_solution_table(ui: QTableWidget, table: TransportTable) -> None:
    assert table.x.shape == table.c.shape == table.alpha.shape

    n_rows, n_cols = table.x.shape
    ui.setRowCount(n_rows + 1)
    ui.setColumnCount(n_cols + 1)

    logging.info(
        f'Updating:\nc = \n{table.c},\nx = {table.x},\na = {table.alpha}\n\n')

    for i_row in range(n_rows):
        for i_col in range(n_cols):
            p = (i_row, i_col)
            item = f'{table.c[p]}'
            item += f'|{table.x[p]}' if table.x[p] is not None else ''
            item += f'\n{table.alpha[p]}' if table.alpha[p] is not None else ''
            ui.setItem(i_row, i_col, QTableWidgetItem(item))

    for i_col in range(n_cols):
        ui.setItem(n_rows, i_col, QTableWidgetItem(str(table.v[i_col])))
    for i_row in range(n_rows):
        ui.setItem(i_row, n_cols, QTableWidgetItem(str(table.u[i_row])))
    ui.setVerticalHeaderItem(2, QTableWidgetItem('A3'))
    ui.setHorizontalHeaderItem(4, QTableWidgetItem('v_i'))


def _update_out_ui(
        i_step: int,
        steps: List[TransportTable],
        table: QTableWidget,
        text: QTextBrowser,
        img_pane: QLabel = None
) -> None:
    logging.info(f'i_step = {i_step}, n_steps: {len(steps)}')

    current_step = steps[i_step]
    text.setText(f'Шаг: {i_step}\nФункция цели: {current_step.loss}')
    _update_solution_table(table, current_step)
    table.resizeRowsToContents()

    if img_pane is not None:
        losses = [s.loss for s in steps]
        image_path = graph.index_graph(losses, i_step)
        image_profile = QtGui.QImage(image_path)  # QImage object
        image_profile = image_profile.scaled(
            400, 400,
            aspectRatioMode=QtCore.Qt.KeepAspectRatio,
            transformMode=QtCore.Qt.SmoothTransformation
        )

        img_pane.setPixmap(QtGui.QPixmap.fromImage(image_profile))
