import copy
import logging
from collections import defaultdict
from typing import Tuple, List, Iterator

import numpy as np  # type: ignore
from src.engine import close_form, first_approx_nw as nw
from src.engine.utils import TransportTable


def calc_u_v(c: np.ndarray, x: np.ndarray) -> Tuple[np.array, np.array]:
    """
    v[j]-u[i]=c[i,j] if x[i,j]>0
    v[0]=0
    To solve these equations, we construct a graph
    with m+n nodes associated with v, u:
        nodes[i] = u[i], 0 <= i < m
        nodes[m+j] = v[j], 0 <= j < n
    where edges represent equations, and then perform a breadth first search.

    :param c: m*n transition matrix
    :param x: m*n matrix; empty cells marked as `None`
    :return: potentials vectors u[m], v[n]
    """
    assert c.shape == x.shape
    (m, n) = c.shape

    # constructing a graph (adjacency list)
    graph = defaultdict(list)
    for i in range(m):
        for j in range(n):
            if x[i, j] is not None:
                graph[i].append(m + j)
                graph[m + j].append(i)

    # performing BFS run
    u, v = np.zeros(m), np.zeros(n)
    visited = [False for _ in range(m + n)]
    stack = [m + 0]
    visited[m + 0] = True  # v[0] := 0 <=> node no. m+0 is visited
    while stack:
        node = stack.pop()
        for adj in graph[node]:  # for all adjacent nodes:
            if not visited[adj]:
                visited[adj] = True
                stack.append(adj)
                if node < m:  # `node` in `u`, `adj` in `v`
                    i = node
                    j = adj - m
                    v[j] = c[i, j] + u[i]
                else:  # `node` in `v`, `adj` in `u`
                    i = adj
                    j = node - m
                    u[i] = v[j] - c[i, j]

    # performing a check: all components of u, v found?
    if False in visited:
        raise Exception("unable to define potentials,"
                        " perhaps the problem is ill-posed")

    return u, v


def calc_alpha(x: np.ndarray, u: np.array, v: np.array) -> np.ndarray:
    m, n = len(u), len(v)
    assert x.shape == (m, n)
    alpha = []
    for i in range(m):
        row = []
        for j in range(n):
            if x[i, j] is None:
                row.append(v[j] - u[i])
            else:
                row.append(None)
        alpha.append(row)
    return np.array(alpha)


def find_loop(i0: int, j0: int, x: np.ndarray) -> List[Tuple[int, int, bool]]:
    # Note: this function changes the value of x[i0, j0] to 0 (have been None)
    # return all nodes in a chain from x[i0, j0] to x[i0, j0] except start, end
    # nodes are returned as (i, j, False) if we came to [i, j] horizontally
    #   or (i, j, True) if vertically
    (m, n) = x.shape
    assert i0 < m and j0 < n

    x[i0, j0] = 0  # was None
    hor = True  # horizontal move (opposite: vertical)
    queue = [(i0, j0, hor)]
    came_from = dict()
    while queue:
        coord = queue.pop(0)
        if coord[2] == hor:  # if next move is horizontal
            i = coord[0]
            adjacent = [(i, j) for j in range(n)
                        if x[i, j] is not None and j != coord[1]]
        else:
            j = coord[1]
            adjacent = [(i, j) for i in range(m)
                        if x[i, j] is not None and i != coord[0]]
        for adj in adjacent:
            if adj not in came_from:
                came_from[adj] = coord
                if adj == (i0, j0):  # if we've found a loop
                    res = [coord]
                    parent = came_from[coord[:-1]]
                    while parent[:-1] != (i0, j0):
                        res.append(parent)
                        parent = came_from[parent[:-1]]
                    return res
                queue.append((*adj, not coord[2]))
    # this code is normally unreachable
    raise Exception("unable to find a loop")


def find_entering_variable(t: TransportTable) -> Tuple[int, int, float]:
    max_diff, i_max, j_max = -float('inf'), -1, -1
    for i in range(t.m):
        for j in range(t.n):
            if t.alpha[i, j] is None:
                continue
            if t.alpha[i, j] - t.c[i, j] > max_diff:
                max_diff = t.alpha[i, j] - t.c[i, j]
                i_max, j_max = i, j
    return i_max, j_max, max_diff


def solve_transportation_potentials(
        c: np.ndarray, x0: np.ndarray, tolerance: float = 1e-04)\
        -> Iterator[TransportTable]:
    u, v = calc_u_v(c, x0)
    alpha = calc_alpha(x0, u, v)
    loss = sum([xi + ci for xi, ci in zip(x0.flatten(), c.flatten())
                if xi is not None])
    t = TransportTable(u=u, v=v, c=c, x0=x0, alpha=alpha, loss=loss)
    while True:
        yield copy.deepcopy(t)
        (i0, j0, max_diff) = find_entering_variable(t)
        if max_diff < tolerance:  # if difference ~0
            break
        loop = find_loop(i0, j0, t.x)

        # choosing an exiting variable
        theta, i1, j1 = float('inf'), -1, -1
        for coord in loop:
            if coord[2] is False and t.x[coord[0], coord[1]] < theta:
                i1, j1 = coord[0], coord[1]
                theta = t.x[i1, j1]

        # Entering, exiting variable and loop with them are found.
        # Now changing plan
        for coord in loop:
            (i, j) = coord[:-1]
            if coord[2] is False:
                t.x[i, j] -= theta
            else:
                t.x[i, j] += theta
        t.x[i0, j0] += theta
        t.x[i1, j1] = None

        # recalculating
        t.u, t.v = calc_u_v(t.c, t.x)
        t.alpha = calc_alpha(t.x, t.u, t.v)
        t.loss = sum([xi * ci for xi, ci in zip(x0.flatten(), c.flatten())
                      if xi is not None])


def solve_open_transportation(
        stock: np.array, need: np.array,
        tariff: np.ndarray, penalty: np.array = None) -> List[TransportTable]:
    if not stock.size > 0 or not need.size > 0 or not tariff.size > 0:
        logging.info("one or more of input vectors/matrices is none")
        return []
    closed_tariff, stock1, need1 = close_form.close_form(
        matrix=tariff, stock=stock, need=need, penalty=penalty)
    starting_plan = nw.first_approx_north_west(closed_tariff, stock1, need1)
    res = solve_transportation_potentials(c=closed_tariff, x0=starting_plan)
    return list(res)
