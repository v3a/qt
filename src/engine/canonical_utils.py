from typing import List
import numpy as np  # type: ignore
import logging
import sys


class CanonicalForm:
    """
    A class that stores input data of linear programming problem in canonical
    form.
    Parameters names corresponds to those in a book by Petuhov (Петухов) et al,
    p. 80, formula (4.25)
    """

    def __init__(self, A: List[List[float]], b: List[float], c: List[float]):
        """
        solving a problem: find x = arg min (c*x), x in S:={x in Rn|Ax=b}
        :param A: m*n matrix
        :param b: vector of size m
        :param c: vector of size n
        """
        self.n = len(c)
        self.m = len(b)
        dimensions_matched = len(A) == self.m
        for row in A:
            dimensions_matched &= len(row) == self.n
        if not dimensions_matched:
            raise Exception("dimensions mismatch")
        self.A, self.b, self.c = A, b, c


class NpCanonicalForm:
    """
    Canonical form representation as NumPy arrays
    (contrary to lists in `CanonicalForm`)
    """

    def __init__(self, cf: CanonicalForm):
        self.n, self.m = cf.n, cf.m
        self.A = np.array([np.array(row) for row in cf.A])
        self.b = np.array(cf.b)
        self.c = np.array(cf.c)


def binomial_grid(n: int, k: int) -> np.ndarray:
    """
    Constructs a square matrix `n-k+1`*`k+1` - rotated by 90 degrees piece
    of Pascal's triangle with corners at (C[0,0], C[`n`,`k`])
    :param n: base of binomial coefficient C[`n`,`k`]
    :param k: index of binomial coefficient C[`n`,`k`]
    :return: a matrix with binomial coefficients
    """
    res = [[1 for _ in range(k + 1)]]
    for row in range(n - k):
        res.append([1])
        for col in range(1, k + 1):
            # С[row,col]=C[row,col-1]+C[row-1,col]
            res[-1].append(res[-1][-1] + res[-2][col])
    return np.array([np.array(row) for row in res])


def subset_by_index(set: np.array, binom_table: np.ndarray,
                    binom_index: int) -> np.array:
    """
    Returns a subset of array `set` of length `n` by number from 0 to C[n,k],
    where C[n,k] is a number in the right-down corner of `binom_table`, k < n.
    For each number `binom_index` algorithm gives a subset, a table is used for
    its calculation.
    :param set: an initial set
    :param binom_table: binomial table created by `binomial_grid` method
    :param binom_index: index of a subset
    :return: the subset (elements ordered as in `set`)
    """
    res, row, col = [], len(binom_table) - 1, len(binom_table[-1]) - 1
    while col > 0:
        if binom_index < binom_table[row, col - 1]:
            res.append(set[row + col - 1])
            col -= 1
        else:
            binom_index -= binom_table[row, col - 1]
            row -= 1
    res = (np.array(res)).astype(int)
    res.sort()
    return res


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    inputData = CanonicalForm([[1, 2, 3], [4, 5, 6]], [7, 8], [9, 10, 11])
    npData = NpCanonicalForm(inputData)
    logging.debug(npData.A)
    logging.debug(npData.b)
    logging.debug(npData.c)
    logging.debug(npData.n)

    logging.debug(binomial_grid(5, 2))
