from typing import TextIO, List, Optional, Tuple
import numpy as np  # type: ignore
import logging

if __name__ == "__main__":
    import sys

    sys.path.append('../../')
from src.engine import potentials, first_approx_nw as nw, close_form
from src.engine.first_approx_min import first_approx_min_elem


def read_array(fin: TextIO) -> np.array:
    exploded_line = fin.readline().strip().split()
    result: List[Optional[float]] = []
    for elem in exploded_line:
        try:
            result.append(float(elem))
        except ValueError:
            result.append(None)
    return np.array(result)


def read_data(
        file_path: str) -> Tuple[np.array, np.array, np.ndarray, np.array]:
    """
    reads canonical form from file in a format:
    a[n] (stock)
    <empty line>
    b[m] (need)
    <empty line>
    c[n,m](tariff matrix)
    <empty line>
    penalty[m]
    :param file_path: relative or absolute path to input file
    :return: (a, b, c, penalty) as numpy arrays
    """
    with open(file_path) as fin:
        a = read_array(fin)
        fin.readline()
        b = read_array(fin)
        n_rows = len(a)
        fin.readline()
        c = np.array([read_array(fin) for _ in range(n_rows)])
        fin.readline()
        penalty = read_array(fin)
    return a, b, c, penalty


def example_transportation_data():
    # credit: Konykhovskiy P. V. (Конюховский П. В).
    a = np.array([27, 20, 43])
    b = np.array([33, 13, 27, 17])
    c_list = [(14, 28, 21, 28),
              (10, 17, 15, 24),
              (14, 30, 25, 21)]
    c = np.array([np.array(row) for row in c_list])
    x0_list = [(27, None, None, None),
               (6, 13, 1, None),
               (None, None, 26, 17)]
    x0 = np.array([np.array(row) for row in x0_list])
    return a, b, c, x0


def solve_example_transp():
    return list(potentials.solve_transportation_potentials(
        *(example_transportation_data())[2:]))


def our_real_problem() -> list:
    stock1, need1, matrix1, _ = read_data("data_samples/our_problem.txt")
    matrix1, stock1, need1 = close_form.close_form(matrix1, stock1, need1)
    new_matrix1 = nw.first_approx_north_west(matrix1, stock1, need1)
    logging.info(new_matrix1)

    new_matrix2 = first_approx_min_elem(matrix1, stock1, need1)
    logging.info(new_matrix2)

    res_gen = potentials.solve_transportation_potentials(matrix1, new_matrix1)
    return list(res_gen)


def real_problem_with_penalty() -> list:
    stock, need, tariff, penalty \
        = read_data("data_samples/our_problem_with_penalty.txt")
    cf, stock1, need1 = close_form.close_form(
        matrix=tariff, stock=stock, need=need, penalty=penalty)
    starting_plan = nw.first_approx_north_west(cf, stock1, need1)

    return list(potentials.solve_transportation_potentials(cf, starting_plan))


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    res = real_problem_with_penalty()
    for t in res:
        logging.info(t.x)
    # res = our_real_problem()
    # x = translinprog.solve_as_canonical(matrix1, stock1, need1)
    # new_matrix2 = first_approx_min_elem(matrix1,stock1,need1, float('inf'))
