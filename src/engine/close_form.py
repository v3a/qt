from typing import Tuple

import numpy as np  # type: ignore


def close_form(matrix: np.ndarray, stock: np.array, need: np.array,
               penalty: np.array = None) -> \
        Tuple[np.ndarray, np.array, np.array]:
    """ Brings the transportation problem to closed-form
         by adding fake provider or consumer"""
    overall_need = np.sum(need)
    overall_stock = np.sum(stock)
    rows, cols = matrix.shape
    delta = overall_stock - overall_need
    if delta < 0:
        if penalty is not None:
            assert len(penalty) == cols
            fake_row = penalty.reshape((1, cols))
        else:
            fake_row = np.zeros(shape=(1, cols))
        matrix = np.append(matrix, fake_row, axis=0)
        stock = np.append(stock, -delta)
    if delta > 0:
        fake_column = np.zeros(shape=(rows, 1))
        matrix = np.append(matrix, fake_column, axis=1)
        need = np.append(need, delta)
    return matrix, stock, need
