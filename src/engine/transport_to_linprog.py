import numpy as np  # type: ignore
from src.engine import simplex
from src.engine.canonical_utils import CanonicalForm, NpCanonicalForm
from src.engine.example import example_transportation_data
import logging
import sys


def solve_as_canonical(c: np.ndarray, a: np.array, b: np.array) -> np.ndarray:
    m, n = len(a), len(b)
    assert c.shape == (m, n)

    c1 = c.flatten()
    A1 = np.zeros([m + n, m * n])

    # horizontal blocks
    for i in range(m):
        for j in range(n):
            A1[i, n * i + j] = 1
    # diagonal blocks
    for k in range(m):
        for s in range(n):
            A1[m + s, n * k + s] = 1
    # throw away one row
    A1 = A1[:-1]
    b1 = np.concatenate([a, b])[:-1]

    ncf = NpCanonicalForm(CanonicalForm(A1, b1, c1))
    # x1 = bruteforce.bruteforce(ncf)
    x0 = simplex.starting_vector(ncf)
    logging.info("--- simplex algorithm---")
    x1 = simplex.simplex_method(ncf, x0).x
    return x1.reshape(c.shape)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    a, b, c, _ = example_transportation_data()
    x = solve_as_canonical(c, a, b)
    logging.info(x)
