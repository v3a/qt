from typing import Tuple, List, Optional
import numpy as np  # type:ignore


def prepare_data(matrix: np.ndarray, old_stock: np.array, old_need: np.array)\
        -> Tuple[np.ndarray, np.array, np.array, np.ndarray]:
    new_matrix = []
    stock = old_stock.copy()
    need = old_need.copy()
    help_matrix = matrix.copy()
    for _ in range(len(matrix)):
        # None defines empty cell
        lst: List[Optional[float]] = [None for _ in range(len(matrix[0]))]
        new_matrix.append(np.array(lst))
    return np.array(new_matrix), stock, need, help_matrix
