import numpy as np  # type: ignore


def first_approx_north_west(matrix: np.ndarray, old_stock: np.array,
                            old_need: np.array) -> np.ndarray:
    from src.engine.first_approx_utils import prepare_data
    new_matrix, stock, need, help_matrix = prepare_data(
        matrix=matrix, old_stock=old_stock, old_need=old_need)

    i = 0
    j = 0
    while i != len(matrix) and j != len(matrix[0]):
        if stock[i] < need[j]:
            new_matrix[i][j] = stock[i]
            need[j] = need[j] - stock[i]
            stock[i] = 0
            i = i + 1
        elif stock[i] > need[j]:
            new_matrix[i][j] = need[j]
            stock[i] = stock[i] - need[j]
            need[j] = 0
            j = j + 1
        else:
            new_matrix[i][j] = need[j]
            stock[i] = 0
            need[j] = 0
            i = i + 1
            j = j + 1
    return np.array(new_matrix)
