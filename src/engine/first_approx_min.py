import numpy as np  # type: ignore


def first_approx_min_elem(matrix: np.ndarray, old_stock: np.array,
                          old_need: np.array) -> np.ndarray:
    from src.engine.first_approx_utils import prepare_data
    new_matrix, stock, need, help_matrix = prepare_data(
        matrix=matrix, old_stock=old_stock, old_need=old_need)

    count = 0
    while count != len(help_matrix):
        min_el = float('inf')
        for i in range(len(help_matrix)):
            for j in range(len(help_matrix[i])):
                if help_matrix[i][j] < min_el and help_matrix[i][j] != 0:
                    min_el = help_matrix[i][j]
        if min_el == float('inf'):
            min_el = 0
            count = count + 1

        min_elements = np.where(help_matrix == min_el)
        if count == len(help_matrix):
            break
        i, j = min_elements[0][0], min_elements[1][0]

        help_matrix[i][j] = float('inf')
        if stock[i] == 0 or need[j] == 0:
            continue

        if stock[i] < need[j]:
            new_matrix[i][j] = stock[i]
            need[j] = need[j] - stock[i]
            stock[i] = 0
        elif stock[i] > need[j]:
            new_matrix[i][j] = need[j]
            stock[i] = stock[i] - need[j]
            need[j] = 0
        else:
            new_matrix[i][j] = need[j]
            stock[i] = 0
            need[j] = 0

    return np.array(new_matrix)
